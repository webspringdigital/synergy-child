/**
 * 1. Define requirements - assign var
 * 2. Require config - paths
 * 3. Export functions
 *   - scss
 *   - minifycss
 *   - scripts
 *   - imagemin - compress images
 * 4. Gulp tasks
 *   - 'copy-assets' copy vendor assets from node_modules to development locations
 *   - 'clean-vendor-assets'
 *   - 'revision'
 *   - 'browserSync' - reload function
 *   - 'watch-bs' - main task
 *       - watches for changes in .scss and .js files
 *       - runs scripts
 *       - reloads browser
 *       - watches for new images
 *       - runs imagemin
 *       - reloads browser
 *     'clean-dist' - delete any files in the /dist directory
 *     'dist' - copy files to the /dist folder for distribution as simple theme
 */

// Define requirements.
var autoprefixer = require( 'autoprefixer' ),
    browserSync = require( 'browser-sync' ).create(),
    cleanCSS = require( 'gulp-clean-css' ),
    concat = require( 'gulp-concat' ),
    del = require( 'del' ),
    gulp = require( 'gulp' ),
    imagemin = require( 'gulp-imagemin' ),
    postcss = require( 'gulp-postcss' ),
    rename = require( 'gulp-rename' ),
    replace = require( 'gulp-replace' ),
    rev = require( 'gulp-rev' ),
    revDel = require( 'rev-del' ),
    sass = require( 'gulp-sass' ),
    sourcemaps = require( 'gulp-sourcemaps' ),
    uglify = require( 'gulp-uglify' );

// Require configuration file to keep your code DRY.
const cfg = require( './gulpconfig.json' );
const paths = cfg.paths;

/**
 * Functions
 */

// Compile SCSS to CSS.
function scss() {
    return gulp.src( paths.sass + '/*.scss' )
        .pipe( sourcemaps.init( {
            loadMaps: true
        } ) )
        .pipe( sass() ).on( 'error', sass.logError )
        .pipe( postcss( [
            autoprefixer()
        ] ) )
        .pipe( sourcemaps.write( undefined, {
            sourceRoot: null
        } ) )
        .pipe( gulp.dest( paths.css ) );
}

// Minify CSS.
function minifycss( done ) {
    gulp.src( paths.css + '/style.css' )
        .pipe( sourcemaps.init( {
            loadMaps: true
        } ) )
        .pipe( cleanCSS( {
            compatibility: '*'
        } ) )
        .pipe( rename( {
            suffix: '.min'
        } ) )
        .pipe( sourcemaps.write( './' ) )
        .pipe( gulp.dest( paths.css ) )

    gulp.src( paths.css + '/custom-editor-style.css' )
        .pipe( sourcemaps.init( {
            loadMaps: true
        } ) )
        .pipe( cleanCSS( {
            compatibility: '*'
        } ) )
        .pipe( rename( {
            suffix: '.min'
        } ) )
        .pipe( sourcemaps.write( './' ) )
        .pipe( gulp.dest( paths.css ) );
    done();
};

// Concatenate scripts and minify them.
function scripts( done ) {
    var scripts = [
        paths.vendor + '/bootstrap/js/bootstrap.js',
        paths.vendor + '/underscores/js/skip-link-focus-fix.js',
        // Adding currently empty javascript file to add on for your own themes´ customizations
        // Please add any customizations to this .js file only!
        paths.vendor + '/underscores/js/custom-javascript.js'
    ];

    gulp.src( scripts )
        .pipe( concat( 'theme.js' ) )
        .pipe( gulp.dest( paths.js ) );

    gulp.src( scripts )
        .pipe( concat( 'theme.min.js' ) )
        .pipe( uglify() )
        .pipe( gulp.dest( paths.js ) );

    done();
}

// Compress image assets.
function imageopt() {
    return gulp.src( paths.imgsrc )
        .pipe( imagemin() )
        .pipe( gulp.dest( paths.img ) );
}

/**
 * Export functions
 */
exports.scss = scss;
exports.minifycss = minifycss;
exports.scripts = scripts;
exports.imageopt = imageopt;

/**
 * Gulp tasks
 */

// Run:
// gulp copy-assets.
// Copy all needed dependency assets files from node_modules to:
// /assets/js.
// /assets/scss.
// /assets/fonts.
// Task runs after 'npm install' or 'npm upate'.

// Copy all assets.
gulp.task( 'copy-assets', function ( done ) {

    // Bootstrap 4 Assets.
    // Copy JS files
    gulp.src( paths.node + 'bootstrap/dist/js/**/*.js' )
        .pipe( gulp.dest( paths.vendor + '/bootstrap/js' ) );

    // Copy Bootstrap SCSS files
    gulp.src( paths.node + 'bootstrap/scss/**/*.scss' )
        .pipe( gulp.dest( paths.vendor + '/bootstrap/scss' ) );

    // Copy Bootstrap 4 License
    gulp.src( paths.node + 'bootstrap/LICENSE' )
        .pipe( gulp.dest( paths.vendor + '/bootstrap' ) );

    // Copy Font Awesome Fonts.
    gulp.src( paths.node + '@fortawesome/fontawesome-free-webfonts/webfonts/*.{eot,svg,ttf,woff,woff2}' )
        .pipe( gulp.dest( paths.fonts ) );

    // Copy Font Awesome 5 SCSS files and license.
    gulp.src( paths.node + '@fortawesome/fontawesome-free-webfonts/scss/*.scss' )
        .pipe( gulp.dest( paths.vendor + '/fontawesome/scss' ) );

    // Copy Font Awesome license to /assets/fonts
    gulp.src( paths.node + '@fortawesome/fontawesome-free-webfonts/LICENSE.txt' )
        .pipe( gulp.dest( paths.fonts ) );

    // Copy _s SCSS files.
    gulp.src( paths.node + 'underscores-for-npm/sass/media/_galleries.scss' )
        .pipe( gulp.dest( paths.vendor + '/underscores/sass/media' ) );

    // Copy _s and Popper JS files to /assets/js.
    // Copy _s JS files.
    gulp.src( [
            paths.node + 'underscores-for-npm/js/*.js',
            '!' + paths.node + 'underscores-for-npm/js/navigation.js'
        ] )
        .pipe( gulp.dest( paths.js ) );

    // Copy Popper JS files.
    gulp.src( paths.node + 'popper.js/dist/umd/popper.min.js' )
        .pipe( gulp.dest( paths.js ) );
    gulp.src( paths.node + 'popper.js/dist/umd/popper.js' )
        .pipe( gulp.dest( paths.js ) );

    done();
} );
// END Copy Assets.

// Run:
// gulp clean-vendor-assets.
// Delete the files distributed by the copy-assets task.
gulp.task( 'clean-vendor-assets', function () {
    return del( [
        paths.fonts + '/fa*.{eot,svg,ttf,woff,woff2}',
        paths.fonts + '/LICENSE.txt',
        paths.js + '/skip-link-focus-fix.js',
        paths.js + '/popper*.js',
        paths.js + '/customizer.js',
        paths.vendor + '/bootstrap',
        paths.vendor + '/fontawesome',
        paths.vendor + '/underscores'
    ] );
} );

// Add cache-busting revision string to rev-manifest.json for adding to...
// enqueued *.min.js and *.min.css files.
gulp.task( 'revision', function ( done ) {
    // by default, gulp would pick `assets/css` as the base,
    // so we need to set it explicitly:
    gulp.src( [ paths.css + '/style.min.css', paths.js + '/theme.min.js' ], {
            base: './'
        } )
        .pipe( rev() )
        .pipe( gulp.dest( './' ) ) // write rev'd assets to build dir.
        .pipe( rev.manifest() )
        .pipe( revDel( {
            dest: './'
        } ) )
        .pipe( gulp.dest( './' ) ); // write manifest to build dir.
    done();
} );

// BrowserSync reload helper function.
function reload( done ) {
    browserSync.reload();
    done();
}

// BrowserSync main task.
gulp.task( 'watch-bs', function ( done ) {
    browserSync.init( cfg.browserSyncWatchFiles, cfg.browserSyncOptions );
    gulp.watch(
        paths.sass + '/**/*.scss',
        gulp.series( scss, minifycss, reload ) );
    gulp.watch( [
            paths.vendor + '/js/**/*.js',
            'js/**/*.js',
            '!js/theme.js',
            '!js/theme.min.js'
        ],
        gulp.series( scripts, reload ) );

    //Inside the watch task.
    gulp.watch( paths.imgsrc, gulp.series( imageopt, reload ) );
    done();
} );

// Delete any file inside the /dist folder.
gulp.task( 'clean-dist', function () {
    return del( [ paths.dist + '/**/*', '!' + paths.dist ] );
} );

// Run:
// gulp dist.
// Copies the files to the /dist folder for distribution as simple theme.
gulp.task( 'dist', gulp.series( 'clean-dist', 'revision', function ( done ) {

    gulp.src( [
            '*',
            '**/*',
            'rev-manifest.json',
            '!' + paths.dist,
            '!' + paths.dist + '/**',
            '!' + paths.distprod,
            '!' + paths.distprod + '/**',
            '!' + paths.imgsrc,
            '!' + paths.sass,
            '!' + paths.sass + '/**',
            '!' + paths.vendor,
            '!' + paths.vendor + '/**',
            '!node_modules',
            '!node_modules/**',
            '!readme.txt',
            '!README.md',
            '!package.json',
            '!package-lock.json',
            '!gulpfile.js',
            '!gulpconfig.json',
            '!CHANGELOG.md',
            '!.travis.yml',
            '!jshintignore',
            '!codesniffer.ruleset.xml'
        ], {
            'buffer': false
        } )
        .pipe( replace( '/assets/js/popper.min.js', paths.node + 'popper.js/dist/umd/popper.min.js', {
            'skipBinary': true
        } ) )
        .pipe( replace( '/assets/js/skip-link-focus-fix.js', paths.node + '/underscores-for-npm/js/skip-link-focus-fix.js', {
            'skipBinary': true
        } ) )
        .pipe( gulp.dest( paths.dist ) );
    done();

} ) );
