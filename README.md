# Synergy Child Theme

**See: Demo coming soon!**

**Website:** Coming soon!

**Synergy (parent theme) Project:** [https://bitbucket.org/webspring/synergy](https://bitbucket.org/webspringdigital/synergy)

## About synergy-child

The Synergy Child theme is a basic child theme for the [Synergy](https://bitbucket.org/webspringdigital/synergy) theme. Synergy Child is based on the [UnderStrap Child Theme](https://github.com/understrap/understrap-child)  theme.

## License

Synergy Child Theme (synergy-child), Copyright 2018 Noel Springer  
synergy-child is distributed under the terms of the GNU GPL version 2.0  
[http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html](http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)

See **Licenses & Credits** below for UnderStrap and other licenses.

## How it works

The Synergy Child theme loads its own `functions.php` file. It uses a `style.css` file in the theme's root to identify itself to WordPress so it can be found and activated in the WordPress admin dashboard.

The Synergy Child theme also loads its own `style.css` file from the `assets/css/` directory using the enqueue method. **The parent theme's stylesheet is not loaded.**

All other files required for the theme to work are shared by the parent theme.

For further information on child themes see [Child Themes](https://developer.wordpress.org/themes/advanced-topics/child-themes/).

## Installation

1. Install the parent theme first:
     * [https://bitbucket.org/webspringdigital/synergy](https://bitbucket.org/webspringdigital/synergy)
2. Upload the synergy-child folder to your themes directory
    * e.g. `wp-content/themes`
3. Go to "Appearance > Themes
4. Activate the Synergy Child Theme

## Editing

Add your own CSS styles to `assets/scss/_custom.scss` or import your own files into `assets/scss/style.scss`.

To overwrite Bootstrap's or Synergy's base variables add your own value to `assets/scss/_overrides.scss`.

For example, the `$primary` variable is used by both Bootstrap and Synergy.

Add your own colour like: `$primary: #009900`. After recompiling the CSS, all elements using the `$primary` variable will use the new colour since it will be incorporated into `assets/css/style.css` and `assets/css/style.min.css`.

## Developing With NPM, Gulp, SASS and Browser Sync

### Install Dependencies

#### Requirements

* **[Node.js](https://nodejs.org/en/)**
  * The recommended way to install Node.js is with [NVM](https://github.com/creationix/nvm), the Node Version Manager.
  * Node.js provids NPM (Node Package Manager).
  * With NPM you can install:
* **Gulp** - install globally.
  * `npm install -g gulp`
* **Browser Sync** - install globally.
  * `npm install -g browser-sync`

Now that the base development tools are installed, navigate to the root of the `synergy-child theme` and run:

* `npm install`

This will install all the required dependencies and run the Gulp task `gulp copy-assets`. The `cppy-assets` task copies the necessary directories and files to their development location.

### Development

To work with the Sass files and compile them on the fly to `assets/css/style.css` and `assets/css/style.min.css` run:

* `gulp watch`

#### Browser Sync [1]

To automatically load (and reload) changes to your CSS in a browser, first change the browser-sync options in the `gulpconfig.json` file to reflect your WordPress local environment:

e.g.

```javascript
  "browserSyncOptions" : {
    "proxy": "localhost/wordpress/",
    "notify": false
  }
};
```

then run: `gulp watch-bs`

## CSS and Sass Files

Some basics about the Sass and CSS files that come with synergy-child:

* The `/style.css` file in the theme's root is only to identify the theme inside of the WordPress dashboard. The file is not enqueued by the theme and does not include any styles.
* The `/css/style.css` file and its minified version `/css/style.min.css` provide all styles. These two files are built from ten SCSS sets and a variables file **`/assets/scss/style.scss`**

```css
@import "overrides";  // 1. Add variables to overwrite Bootstrap or Synergy variables here.
@import "../vendor/bootstrap/scss/bootstrap";// 2. Loads Bootstrap 4
@import "theme/synergy"; // 3. Adds some necessary WordPress styles and styles for integrating Bootstrap with Underscores.
@import "theme/woocommerce"; // 4. Fixes for Woocommerce display. Comment out if not using Woocommerce.
//Optional files * If you don't use the corresponding scripts/fonts comment them out
@import "../vendor/fontawesome/scss/fontawesome"; // 5. Font Awesome icon fonts.
@import "../vendor/fontawesome/scss/fa-brands"; // 6. Font Awesome brand icon fonts.
@import "../vendor/fontawesome/scss/fa-regular"; // 7. Font Awesome regular icon fonts.
@import "../vendor/fontawesome/scss/fa-solid"; // 8. Font Awesome solid icon fonts.
@import "../vendor/underscores/sass/media/galleries"; // 9. Underscores media styles.

// Any additional imported files
// @import "theme/contact-form7/contact-form7"; // Contact Form 7 - Bootstrap 4 support. Uncomment if using the Contact Form 7 plugin.
@import "custom"; // 10. This is where you can add your own design.
```

* Your design:
  * Add your styles to the `/assets/scss/_custom.scss` file
  * And your variables to the `/assets/scss/_overrides.scss`
  * Or add other .scss files into the `assets/scss` directory and `@import` them into `/assets/scss/_style.scss`

## RTL styles

Add a new file to the themes root folder called rtl.css. Add all alignments to this file according to this description:
[https://codex.wordpress.org/Right_to_Left_Language_Support](https://codex.wordpress.org/Right_to_Left_Language_Support)

## Page Templates

### Blank Template

The `blank.php` template is useful when working with various page builders and can be used as a starting blank canvas.

### Empty Template

The `empty.php` template displays a header and a footer only. A good starting point for landing pages.

### Full Width Template

The `fullwidthpage.php` template has full width layout without a sidebar.

## Changelog

See [CHANGELOG.md](CHANGELOG.md)

## Footnotes

[1] Visit [https://browsersync.io/](https://browsersync.io/) for more information on Browser Sync

## Licenses & Credits

* UnderStrap WordPress Theme: [https://github.com/understrap/understrap/blob/master/LICENSE.md](https://github.com/understrap/understrap/blob/master/LICENSE.md)
  * Copyright 2013-2017 Holger Koenemann
* Underscores (_s): [https://github.com/Automattic/_s/blob/master/LICENSE](https://github.com/Automattic/_s/blob/master/LICENSE)
* Font Awesome: [https://fontawesome.com/license](https://fontawesome.com/license) (Font: SIL OFL 1.1, CSS: MIT License)
* Bootstrap: [https://getbootstrap.com](https://getbootstrap.com) | [https://github.com/twbs/bootstrap/blob/master/LICENSE](https://github.com/twbs/bootstrap/blob/master/LICENSE) (Code licensed under MIT documentation under CC BY 3.0.)
* jQuery: [https://jquery.org](https://jquery.org) | (Code licensed under MIT)
* WP Bootstrap Navwalker by Edward M`/wp-content/themes/`cIntyre: [https://github.com/twittem/wp-bootstrap-navwalker](https://github.com/twittem/wp-bootstrap-navwalker) | GNU GPL
* Bootstrap Gallery Script based on Roots Sage Gallery: [https://github.com/roots/sage/blob/5b9786b8ceecfe717db55666efe5bcf0c9e1801c/lib/gallery.php](https://github.com/roots/sage/blob/5b9786b8ceecfe717db55666efe5bcf0c9e1801c/lib/gallery.php)
