# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.3] - 2018-07-08

### Added

* Add CODE-OF-CONDUCT.md file
* Add eslint config files and config for .jsbeatify

### Changed

* Update for Gulp 4 from parent theme
* Update .gitignore for new architecture

### Removed

* Remove assets from old locations

## [1.0.2] - 2018-06-14

### Changed

* Update version number in root `style.css`.

## [1.0.1] - 2018-06-14

### Added

* Add rev cache busting to theme css and js.

### Fixed

* Delete extraneous '.css', '.css.map' files in vendor directory.

## [1.0.0] - 2018-06-14

### Added

* LICENSE.md
* CHANGELOG.md
* `assets` directory
* `assets/vendor` directory

### Changed

* Move assets: `css`, `img`, `js`, `fonts` and `vendor` directories to the `assets` directory
* Update paths as needed
* Update `README.md`
* Update `/style.css`

### Removed

* `src` directory moved to `assets/vendor`.
* `sass` directory. Vendor Sass files moved to the relevant `assets/vendor` directory. Other theme Sass files moved to `assets/scss`

### Note

Tags earlier than 1.0.0 are by the understrap-child project [https://github.com/understrap/understrap-child](https://github.com/understrap/understrap-child)
