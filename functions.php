<?php
/**
 * Enqueue child theme styles and scripts.
 * Based on UnderStrap Child theme.
 *
 * @package  Synergy Child
 * @author   Noel Springer, Holger Koenemann
 * @license  http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

add_action( 'wp_enqueue_scripts', 'synergy_remove_scripts', 20 );
/**
 * Removes the parent themes stylesheet and scripts from inc/enqueue.php.
 */
function synergy_remove_scripts() {
	wp_dequeue_style( 'synergy-styles' );
	wp_deregister_style( 'synergy-styles' );

	wp_dequeue_script( 'synergy-scripts' );
	wp_deregister_script( 'synergy-scripts' );
}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
/**
 * Enqueue styles and scripts for child theme.
 */
function theme_enqueue_styles() {

	// Get the theme data.
	$the_theme = wp_get_theme();
	wp_enqueue_style( 'child-synergy-styles', trailingslashit( get_stylesheet_directory_uri() ) . asset_path( 'assets/css/style.min.css' ), array(), $the_theme->get( 'Version' ) );
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'popper-scripts', trailingslashit( get_template_directory_uri() ) . 'js/popper.min.js', array(), false );
	wp_enqueue_script( 'child-synergy-scripts', trailingslashit( get_stylesheet_directory_uri() ) . asset_path( 'assets/js/theme.min.js' ), array(), $the_theme->get( 'Version' ), true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

/**
 * Load child theme text domain.
 */
function add_child_theme_textdomain() {
	load_child_theme_textdomain( 'synergy-child', trailingslashit( get_stylesheet_directory() ) . 'languages' );
}
add_action( 'after_setup_theme', 'add_child_theme_textdomain' );
